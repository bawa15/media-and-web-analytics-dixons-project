Course project for Media and Web Analytics course. Scraped reviews from websites like Reevoo, analysed the sentiment of these reviews and developed classifiers to classify these as "delivery" or "customer service". It was a binary classification problem.

Language: Python
