from pycorenlp import StanfordCoreNLP
from nltk.corpus import stopwords
import nltk
from nltk.corpus import wordnet as wn
from nltk.corpus import sentiwordnet as swn
from textblob import Word
from textblob import TextBlob
import csv
import time
from nltk.stem import WordNetLemmatizer


def AnalyseSentiments():

	nlp = StanfordCoreNLP('http://localhost:9000')
	Reviews = []
	operations = {'annotators': 'tokenize,lemma,pos,sentiment',
             'outputFormat': 'json'}
	allReviewData = []
	#initialWriteToFile()
	l = -1
	fileName = "Test2.csv"
	with open(fileName, 'r') as f:
		reader = csv.reader(f, delimiter=',')
		for row in reader:
			l += 1
			try:
				if l == 494:						#Breaks a the last row. Avoids any whitespace
					break
				tokens = nltk.word_tokenize(row[0])
				Reviews.append(row[0])
				if not len(tokens)<2:
					allReviewData.append(row)
				print(str(l))
			except(IndexError):
				pass
	filtered_Reviews = []
	tokensU = []
	NoOfTokens = 0
	u = 0
	print("Finished reading")
	for Review in Reviews:
		tokens = nltk.word_tokenize(Review)
		for tok in tokens:
			tokensU.append(tok)
		NoOfTokens += len(tokens)
	tokensU = set(tokensU)
	print(NoOfTokens)
	#u = set()
	print(len(tokensU))
	print(str(len(set(NoOfTokens))))
AnalyseSentiments()
